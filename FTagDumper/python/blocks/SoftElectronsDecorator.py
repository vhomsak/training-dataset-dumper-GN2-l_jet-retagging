from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class SoftElectronsDecorator(BaseBlock):
    """Do electrons decoration and jet association.

    See [SoftElectronDecoratorAlg.h](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/SoftElectronDecoratorAlg.h)
    See [SoftElectronTruthDecoratorAlg.h](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/SoftElectronTruthDecoratorAlg.h)
    See [FTagGhostElectronAssociationAlg.h](https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/FTagGhostElectronAssociationAlg.h)

    Parameters
    ----------
    jet_collection : str
        The name of the jet collection to which the electrons are associated. If not provided, it will be taken from the dumper config.
    electrons_collection : str
        The name of the electron collection to decorate. Default is "GhostFTagElectrons".
    decorate_vars : bool
        Whether to decorate the variables. Default is False.
    decorate_truth_vars : bool
        Whether to decorate the truth variables. Default is False.
    link_electrons : bool
        Whether to link the electrons to the jet collection. Default is False.
    """

    jet_collection: str = None
    electrons_collection: str = "GhostFTagElectrons"
    decorate_vars: bool = False
    decorate_truth_vars: bool = False
    link_electrons: bool = False


    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config["jet_collection"]

    def to_ca(self):
        ca = ComponentAccumulator()
        if self.decorate_vars:
            ca.addEventAlgo(
                CompFactory.FlavorTagDiscriminants.SoftElectronDecoratorAlg(
                    'SoftElectronDecoratorAlg'
                )
            )
        if self.decorate_truth_vars:
            ca.addEventAlgo(
                CompFactory.FlavorTagDiscriminants.SoftElectronTruthDecoratorAlg(
                    'SoftElectronTruthDecoratorAlg'
                )
            )
        if self.link_electrons:
            ca.addEventAlgo(
                CompFactory.FlavorTagDiscriminants.FTagGhostElectronAssociationAlg(
                    'FTagGhostElectronAssociationAlg',
                    jetContainer='AntiKt4EMPFlowJets',
                    outElectrons="AntiKt4EMPFlowJets.GhostFTagElectrons",
                )
            )
        
        return ca