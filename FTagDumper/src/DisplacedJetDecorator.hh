#ifndef DISPLACED_JET_DECORATOR_HH
#define DISPLACED_JET_DECORATOR_HH

#include "xAODBTagging/BTaggingContainerFwd.h"
#include "xAODJet/JetContainerFwd.h"
#include "xAODTruth/TruthParticleFwd.h"

#include "AthLinks/ElementLink.h"

#include <vector>

class DisplacedJetDecorator {
 public:
  using AE = SG::AuxElement;
  typedef ElementLink<xAOD::IParticleContainer> PartLink;
  typedef std::vector<PartLink> PartLinks;

  DisplacedJetDecorator(const std::string& link_name, const std::vector<int>& pdgIdList);

  std::tuple<float, float, float> getJetVar(const PartLinks& part_links) const;
  const xAOD::TruthParticle* checkProduction( const xAOD::TruthParticle & truthPart ) const;

  void decorate(const xAOD::Jet& jet) const;

 private:
  AE::ConstAccessor<PartLinks> m_truth_particle_links;
  AE::Decorator<int> m_deco_is_displaced;
  AE::Decorator<float> m_deco_disp_pt_frac;
  AE::Decorator<float> m_deco_parent_llp_massGeV;
  AE::Decorator<float> m_deco_parent_llp_lifetime;

  std::vector<int> m_pdgIdList;
};
#endif
