#ifndef EVENT_WRITER_CONFIG_HH
#define EVENT_WRITER_CONFIG_HH

#include "VariablesByType.hh"

#include <string>

struct EventWriterConfig {
  std::string name = "eventwise";
  bool force_full_precision = false;
  VariablesByCompression vars;
};

#endif
