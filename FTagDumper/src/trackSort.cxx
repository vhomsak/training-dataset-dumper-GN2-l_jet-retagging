#include "trackSort.hh"

#include "xAODTracking/TrackParticle.h"
#include "FlavorTagDiscriminants/BTagTrackIpAccessor.h"

namespace {

  using TP = xAOD::TrackParticle;
  using J = xAOD::Jet;

  std::function<float(const TP&, const J&)> sortvalue(
    TrackSortOrder order, const std::string& p)
  {
    using Acc = BTagTrackIpAccessor;
    switch(order) {
    case TrackSortOrder::ABS_D0_SIGNIFICANCE:
      return [a=Acc(p)](const TP& t, const J&) {
        return std::abs(a.d0(t) / a.d0Uncertainty(t));
      };
    case TrackSortOrder::ABS_D0:
      return [a=Acc(p)](const TP& t, const J&) {
        return std::abs(a.d0(t));
      };
    case TrackSortOrder::D0_SIGNIFICANCE:
      return [a=Acc(p)](const TP& t, const J& j) {
        return a.getSignedIp(t,j).ip3d_signed_d0_significance;
      };
    case TrackSortOrder::ABS_BEAMSPOT_D0:
      return [](const TP& t, const J&) {
        return std::abs(t.d0());
      };
    case TrackSortOrder::PT:
      return [](const TP& t, const J&) {
        return t.pt();
      };
    default: throw std::logic_error("undefined sort order");
    }
  }
}

TrackSort trackSort(TrackSortOrder order, const std::string prefix) {
  return [sv=sortvalue(order, prefix)] (const TPV& tv, const xAOD::Jet& j) {
    std::vector<std::pair<float, const xAOD::TrackParticle*>> stv;
    for (const auto* t: tv) {
      // we sort in ascending order: multiply sort values by -1 to get
      // the largest first.
      stv.emplace_back(-sv(*t, j), t);
    }
    std::sort(stv.begin(), stv.end());
    TPV out;
    for (const auto& tpair: stv) {
      out.push_back(tpair.second);
    }
    return out;
  };
}
