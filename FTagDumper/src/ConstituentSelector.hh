#ifndef CONSTITUENT_SELECTOR_HH
#define CONSTITUENT_SELECTOR_HH

#include "xAODPFlow/TrackCaloCluster.h"
#include "xAODJet/Jet.h"

class ConstituentSelector
{
public:
  ConstituentSelector();
  typedef std::vector<const xAOD::TrackCaloCluster*> TCCs;
  TCCs get_constituents(const xAOD::Jet& jet) const;
private:
};

#endif
