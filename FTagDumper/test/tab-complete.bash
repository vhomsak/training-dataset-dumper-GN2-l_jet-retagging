# function to add tab complete to unit tests

# set the environment variable TABTEST to dump output to that file,
# and read with `tail -f`

# Check if we are in BASH, or not interactive, get out of here
if [ "x${BASH_SOURCE[0]}" = "x" ] || ! [[ $- == *i* ]]; then
    return
fi

# add a function to help with the tests
function _test_dumper() {
    local current=${COMP_WORDS[COMP_CWORD]}
    local previous=${COMP_WORDS[COMP_CWORD-1]}
    local log=${TABTEST-/dev/null}
    echo -n "current: ${current}, previous: ${previous}," >> ${log}
    # try to use a few special cases first
    if [[ ${previous} == '-d' ]]; then
        COMPREPLY=($(compgen -d -- ${current}))
        echo " dir" >> ${log}
        return 0
    fi
    # if we're down here just ask the test script for options
    local opts=$(test-dumper -c)
    COMPREPLY=($(compgen -W "${opts}" -- ${current}))
    echo " all options" >> ${log}
}
complete -F _test_dumper test-dumper
