#ifndef JET_ASSOCATION_WRITER_CONFIG_H
#define JET_ASSOCATION_WRITER_CONFIG_H

#include "JetConstituentWriterConfig.h"

struct JetLinkWriterConfig
{
  enum class Type {UNKNOWN, FlowElement, IParticle, Electron, CaloCluster};
  Type type = Type::UNKNOWN;
  JetConstituentWriterConfig constituents;
  std::string accessor;
};

#endif
